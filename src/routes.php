<?php

use \App\Controllers\{ControllerCook, ControllerUser};

// Routes

$app->group('/api', function () {
    /**
     * Auth user (get Token), reg user, logout
     */
    $this->get('/user', ControllerUser::class . ':login')->setName('auth.login');
    $this->post('/user/reg', ControllerUser::class . ':registration')->setName('auth.registration');
    $this->get('/user/logout', ControllerUser::class . ':logout')->setName('auth.registration');

    /**
     * Get cook data or remove
     */
    $this->get('/cook', ControllerCook::class . ':getcook')->setName('cook.get');
    $this->delete('/cook/delete', ControllerCook::class . ':deletecook')->setName('cook.delete');
    
    /**
     * Add cook or update
     */
    $this->post('/cook', ControllerCook::class . ':addcook')->setName('cook.add');
    $this->put('/cook/update', ControllerCook::class . ':updatecook')->setName('cook.update');
});
