<?php
namespace App\Models;

class Model {
    protected $db = false,
              $cache = false;
    
    public function __construct() {
        $this->db = new \PDO("pgsql:host=/var/run/postgresql;port=5432;dbname=test;user=test;password=pwd");
        $this->cache = new \Memcache;
        $this->cache->connect('10.0.0.111', 53401);
    }
}
