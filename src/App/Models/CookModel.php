<?php
namespace App\Models;
use App\Models\UsersModel;
use Slim\Http\UploadedFile;


class CookModel extends UsersModel {
    private $params = [],
            $upload_dir;
    
    public function __construct(array $params = []) {
        $this->params = $params;
        $this->upload_dir = 'uploads/photos';
        return parent::__construct();
    }

    /**
     * Get recipe list
     * @param string $user username
     * @return array
     */
    public function getCook(string $user = ""): array {
        $search = $this->params['text'];
        $id = $this->params['id'];
        $result = [];

        $query = $this->db->prepare('SELECT * FROM cooktable WHERE (title LIKE :search OR ingredients LIKE :search OR text LIKE :search OR id=:id) AND author=:user');
        $query->bindParam(':search', $search);
        $query->bindParam(':id', $id);
        $query->bindParam(':user', $user);
        $query->execute();
        foreach ($query->fetchAll() as $res) {
            $result[] = [
                'id' => $res['id'],
                'name' => $res['title'],
                'ingredients' => $res['ingredients'],
                'description' => $res['text'],
                'photo' => $res['photo'],
                'author' => $res['author']
            ];
        }
        return $result;
    }
    
    /**
     * Delete recipe
     * @param string $user
     * @return bool
     */
    public function deleteCook(string $user = ""): bool {
        $id = $this->params['id'];
        $query = $this->db->prepare('SELECT id FROM cooktable WHERE id=:id and author=:user');
        $query->bindParam(':id', $id);
        $query->bindParam(':user', $user);
        $query->execute();
        $getid = $query->fetch();
        if (!empty($getid['id'])) {
            $query = $this->db->prepare('DELETE FROM cooktable WHERE id=:id AND author=:user');
            $query->bindParam(':id', $id);
            $query->bindParam(':user', $user);
            return $query->execute();
        } else {
            return false;
        }
    }
    
    /**
     * Add new recipe
     * @param string $user
     * @param array $files
     * @return int
     */
    public function addCook(string $user = "", array $files = []): int {
        $photo = $files['photo'];
        $filename = "";
        if ($photo->getError() === UPLOAD_ERR_OK) {
            $filename = $this->movefile($this->upload_dir, $photo);
        }

        $query = $this->db->prepare('INSERT INTO cooktable (title, ingredients, text, photo, created_at, author) VALUES (:name, :ingredients, :description, :photo, :date, :author)');
        $query->bindParam(':name', $this->params['name']);
        $query->bindParam(':ingredients', $this->params['ingredients']);
        $query->bindParam(':description', $this->params['description']);
        $query->bindParam(':date', date('Y-m-d H:i:s'));
        $query->bindParam(':author', $user);
        $query->bindParam(':photo', $filename);
        $query->execute();

        return $this->db->lastInsertId();
    }
    
    /**
     * Update recipe
     * @param string $user
     * @param array $files
     * @return int
     */
    public function updateCook(string $user = "", array $files = []): int {
        $photo = $files['photo'];
        $query = $this->db->prepare('SELECT * FROM cooktable WHERE id=:id and author=:user LIMIT 1');
        $query->bindParam(':id', $this->params['id']);
        $query->bindParam(':user', $user);
        $query->execute();
        $old_query = $query->fetch();
        $photo_link = $old_query['photo'];
        
        if ($photo->getError() === UPLOAD_ERR_OK) {
            if (!empty($photo_link)) {
                unlink(BASE_DIR.$photo_link);
            }
            $photo_link = $this->movefile($this->upload_dir, $photo);
        }

        $name = $this->params['name'] ?? $old_query['title'];
        $ingredients = $this->params['ingredients'] ?? $old_query['ingredients'];
        $description = $this->params['description'] ?? $old_query['text'];

        $query = $this->db->prepare('UPDATE cooktable SET title=:name, ingredients=:ingredients, text=:description, photo=:photo WHERE id=:id AND author=:user');
        $query->bindParam(':name', $name);
        $query->bindParam(':ingredients', $ingredients);
        $query->bindParam(':description', $description);
        $query->bindParam(':id', $this->params['id']);
        $query->bindParam(':user', $user);
        $query->bindParam(':user', $user);
        $query->bindParam(':photo', $photo_link);
        return $query->execute();
    }
    
    /**
     * Move image
     * @param string $dir
     * @param UploadedFile $file
     * @return string
     */
    private function movefile(string $dir = "", UploadedFile $file): string {
        $extension = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);
        $filename = "";
        switch ($extension) {
            case 'jpg':
            case 'png':
            case 'gif':
            case 'jpeg':
                $basename = bin2hex(random_bytes(8));
                $filename = sprintf('%s.%0.8s', $basename, $extension);
                $file->moveTo(BASE_DIR.'/'.$dir . '/' . $filename);
                $filename = $dir.'/'.$filename;
            break;

        }
        return $filename;
    }
}
