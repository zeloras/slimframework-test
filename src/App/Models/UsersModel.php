<?php
namespace App\Models;
use App\Models\Model;


class UsersModel extends Model {
    private $params = [],
            $login = "",
            $password = "";
    /**
     * 
     * @param array $params
     * @return type
     */
    public function __construct(array $params = []) {
        $this->params = $params;
        $this->login = $this->params['user'] ?? "";
        $this->password = $this->params['password'] ?? "";
        return parent::__construct();
    }
    
    /**
     * Check user
     * @return bool
     */
    public function checkLogin(): bool {

        if (strlen($this->login) > 2 && strlen($this->password) > 2) {
            $query = $this->db->prepare('SELECT id FROM users WHERE username=:username AND password=:password LIMIT 1');
            $query->bindParam(':username', $this->login);
            $query->bindParam(':password', $this->password);
            $query->execute();
            
            $result = $query->fetch();
            $id = $result['id'] ?? 0;
            return (bool)$id;

        }
        
        return false;
    }
    
    /**
     * Generate api key if not exists
     * @return string
     */
    public function getHash(): string {
        $hash = "";
        if ($this->checkLogin()) {
            $hash = md5($this->login.$this->password.time());
            $this->cache->set($hash, $this->login, MEMCACHE_COMPRESSED, 3600);
        }
        return $hash;
    }
    
    /**
     * Remove login key
     * @return bool
     */
    public function removeHash(): bool {
        $status = false;
        $hash = $this->params['api'];
        
        if (!empty($hash)) {
            if ($this->cache->get($hash)) {
                $status = $this->cache->delete($hash);
            }
        }
        
        return $status;
    }

    /**
     * Add new user
     * @return bool
     */
    public function addUser(): bool {
        $query = $this->db->prepare('INSERT INTO users (username, password, created_at) VALUES (:username, :password, :date)');
        $query->bindParam(':username', $this->login);
        $query->bindParam(':password', $this->password);
        $query->bindParam(':date', date('Y-m-d H:i:s'));
        return $query->execute();
    }
    
    /**
     * Check api key
     * @return array
     */
    public function checkApiKey() : array {
        $return = ['status' => false];

        if (!empty($this->params['api'])) {
            $hash = $this->cache->get($this->params['api']);
            if ($hash) {
                return ['status' => true, 'username' => $hash];
            }
        }
        
        return $return;
    }
}

