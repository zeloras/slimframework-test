<?php
namespace App\Controllers;
use Slim\Http\{Request, Response};
use App\Models\{CookModel, UsersModel};


class ControllerCook {
    
    private $username = "";

    /**
     * Get recipe
     * @param Request $request
     * @param Response $response
     */
    public function getcook(Request $request, Response $response) {
        $params = $request->getQueryParams();
        $return = $this->checkKey($params);

        if ((bool)$return['status']) {
            if (!empty($params['text']) || !empty($params['id'])) {
                $model = new CookModel($params);
                $result = $model->getCook($this->username);
                $return = [
                    'status' => 1,
                    'message' => $result,
                ];
            } else {
                $return = [
                    'status' => 0,
                    'message' => [],
                    'error' => 'text field or id field is empty'
                ];
            }
        }

        return $response->withJson($return);
    }
    
    /**
     * Delete record
     * @param Request $request
     * @param Response $response
     */
    public function deletecook(Request $request, Response $response) {
        $params = $request->getQueryParams();
        $return = $this->checkKey($params);

        if ((bool)$return['status']) {
            if (!empty($params['id'])) {
                $model = new CookModel($params);
                $result = $model->deleteCook($this->username);
                if ($result) {
                    $return = [
                        'status' => 1,
                        'message' => 'record deleted',
                    ];
                } else {
                    $return = [
                        'status' => 0,
                        'message' => [],
                        'error' => 'record with current id not exists'
                    ];
                }
            } else {
                $return = [
                    'status' => 0,
                    'message' => [],
                    'error' => 'id field is empty'
                ];
            }
        }

        return $response->withJson($return);
    }
    
    /**
     * Add new record
     * @param Request $request
     * @param Response $response
     */
    public function addcook(Request $request, Response $response) {
        $params = $request->getParams();
        $return = $this->checkKey($params);

        if ((bool)$return['status']) {
            if (!empty($params['name']) && !empty($params['description']) && !empty($params['ingredients'])) {
                $model = new CookModel($params);
                $result = $model->addCook($this->username, $request->getUploadedFiles());
                $return = [
                    'status' => 1,
                    'message' => 'record added id: '.$result,
                ];
            } else {
                $return = [
                    'status' => 0,
                    'message' => [],
                    'error' => 'name or description or ingredients field is empty'
                ];
            }
        }

        return $response->withJson($return);
    }
    
    /**
     * Set new data for record
     * @param Request $request
     * @param Response $response
     */
    public function updatecook(Request $request, Response $response) {
        $params = $request->getParams();
        $return = $this->checkKey($params);

        if ((bool)$return['status']) {
            if (!empty($params['id'])) {
                $model = new CookModel($params);
                $model->updateCook($this->username, $request->getUploadedFiles());
                $return = [
                    'status' => 1,
                    'message' => 'record update',
                ];
            } else {
                $return = [
                    'status' => 0,
                    'message' => [],
                    'error' => 'id field is empty'
                ];
            }
        }

        return $response->withJson($return);
    }
    
    /**
     * Check api key and return status array
     * @param array $params
     */
    private function checkKey(array $params = []) {
        $authModel = new UsersModel($params);
        $checkdata = $authModel->checkApiKey();
        if ($checkdata['status']) {
            $this->username = $checkdata['username'];

            $return = [
                'status' => 1
            ];
        } else {
            $return = [
                'status' => 0,
                'message' => [],
                'error' => "Api key worng"
            ];
        }
        
        return $return;
    }
}
