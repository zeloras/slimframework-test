<?php
namespace App\Controllers;
use Slim\Http\{Request, Response};
use App\Models\UsersModel;

class ControllerUser {
    
    /**
     * Route for get hash
     * @param Request $request
     * @param Response $response
     * @return array
     */
    public function login(Request $request, Response $response) {
        $return = [
            'status' => 0,
            'message' => [],
            'error' => "can't get has, login or password was wrong"
        ];
        $params = $request->getQueryParams();
        $model = new UsersModel($params);

        $hash = $model->getHash();
        if (strlen($hash) > 5) {
            $return = [
                'status' => 1,
                'message' => [
                    'hash' => $hash
                ]
            ];
        }
        
        return $response->withJson($return);
    }
    
    /**
     * Register user
     * @param Request $request
     * @param Response $response
     */
    public function registration(Request $request, Response $response) {
        $return = [
            'status' => 0,
            'message' => [],
            'error' => "can't register user, user already exists"
        ];
        $params = $request->getParams();
        $model = new UsersModel($params);
        $checklogin = $model->checkLogin();
        if (!$checklogin) {
            if ($model->addUser()) {
                $return = [
                    'status' => 1,
                    'message' => [
                        'registration' => true,
                        'hash' => $model->getHash()
                    ]
                ];
            } else {
                $return['error'] = "can't register user, server error";
            }
        }

        return $response->withJson($return);
    }
    
    /**
     * Logout user and remove key
     * @param Request $request
     * @param Response $response
     */
    public function logout(Request $request, Response $response) {
        $return = [
            'status' => 0,
            'message' => [],
            'error' => "already logout"
        ];
        $params = $request->getParams();
        $model = new UsersModel($params);
        $rmrf = $model->removeHash();
        if ($rmrf) {
            $return = [
                'status' => 1,
                'message' => ['state' => 'logout']
            ];
        }

        return $response->withJson($return);
    }
}
