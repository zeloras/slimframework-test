# Информация по API

---

**Регистрация пользователя**

Поля: username, password

Обязательные поля: username, password

Запрос post, url /api/user/reg, возвращает api ключ


---
**Авторизация пользователя**

Поля: username, password

Обязательные поля: username, password

Запрос get, url /api/user, возвращает api ключ

---
**Уничтожение api ключа**

Поля: api

Обязательные поля: api

Запрос get, url /api/user/logout, возвращает массив с информацией

---
**Получение рецептов пользователя**

Поля: api, (id, name) поиск может быть как по id так и по ключевому слову

Обязательные поля: api, id/name

Запрос get, url /api/cook, возвращает массив с информацией

---
**Удаление рецепта пользователя**

Поля: api, id

Обязательные поля: api, id

Запрос delete, url /api/cook/delete, возвращает массив с информацией о удалении

---
**Добавление рецепта**

Поля: api, name, ingredients, description, photo(файл)

Обязательные поля: api, name, description

Запрос post, url /api/cook, возвращает массив с информацией о добавлении рецепта и его id

---
**Обновление рецепта**

Поля: api, id, name, ingredients, description, photo(файл)

Обязательные поля: api, id

Запрос put, url /api/cook/update, возвращает массив с информацией о обновлении

---
#Пример ответа сервера.

**Ошибка**:
[
    'status' => 0,
    'message' => [],
    'error' => 'error text'
]

---
**Все впорядке**:
[
    'status' => 1,
    'message' => ['не пустой массив']
]
